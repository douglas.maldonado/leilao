package br.com.leilao;

import com.sun.xml.internal.ws.assembler.jaxws.MustUnderstandTubeFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTest {

    Leiloeiro leiloeiro;
    Leilao leilao;
    Lance lance;
    Usuario usuario;
    List<Lance> listaLance = new ArrayList<Lance>();

    @BeforeEach
    public void setUp(){
        leiloeiro = new Leiloeiro();
        leilao = new Leilao();


        leiloeiro.setNome("Leiloes SA");

        lance = new Lance();
        usuario = new Usuario();
        usuario.setNome("Usuario 1");
        usuario.setId((long)1);
        lance.setUsuario(usuario);
        lance.setValorLance(200.00);
        listaLance.add(lance);

        lance = new Lance();
        usuario = new Usuario();
        usuario.setNome("Usuario 2");
        usuario.setId((long)2);
        lance.setUsuario(usuario);
        lance.setValorLance(300.00);
        listaLance.add(lance);

        lance = new Lance();
        usuario = new Usuario();
        usuario.setNome("Usuario 3");
        usuario.setId((long)2);
        lance.setUsuario(usuario);
        lance.setValorLance(400.00);
        listaLance.add(lance);

        leilao.setLanceLista(listaLance);

    }

    @Test
    public void testarRetornarMaiorLance() {
        leiloeiro.setNome("Leiloes SA");
        Lance lanceRetorno = leiloeiro.retornarMaiorLance(leilao);

        Assertions.assertEquals("Usuario 3", lanceRetorno.getUsuario().getNome());
        Assertions.assertEquals(400, lanceRetorno.getValorLance());
    }


}
