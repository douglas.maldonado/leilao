package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTest {

    Leilao leilao;
    Usuario usuario;
    Lance lance;
    List<Lance> lanceTeste = new ArrayList<Lance>();



    @BeforeEach
    public void setUp(){
        leilao = new Leilao();
        usuario = new Usuario();
        lance = new Lance();

        usuario.setId((long)1);
        usuario.setNome("Douglas");
        lance.setUsuario(usuario);
        lance.setValorLance(200.00);
        lanceTeste.add(lance);
        leilao.adicionarLance(lance);

        usuario = new Usuario();
        lance = new Lance();

        usuario.setId((long)2);
        usuario.setNome("Jovem");
        lance.setUsuario(usuario);
        lance.setValorLance(300.00);
        lanceTeste.add(lance);
        leilao.adicionarLance(lance);

        usuario = new Usuario();
        lance = new Lance();
    }

    @Test
    public void testarAdicionarLance(){
        boolean adicionou = false;

        usuario.setId((long) 3);
        usuario.setNome("Testando");
        lance.setUsuario(usuario);
        lance.setValorLance(400.00);
        lanceTeste.add(lance);

        List<Lance> lanceRetorno = leilao.adicionarLance(lance);

        for(int i=0; i < lanceRetorno.size(); i++){
            if(lanceRetorno.get(i).equals(lanceTeste.get(i))){
                adicionou = true;
            }
        }

        Assertions.assertTrue(adicionou);
    }

    @Test
    public void testarAdicionarLanceMenor(){
        usuario.setId((long) 3);
        usuario.setNome("Testando");
        lance.setUsuario(usuario);
        lance.setValorLance(100.00);
        lanceTeste.add(lance);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarLance(lance);});
    }
}
