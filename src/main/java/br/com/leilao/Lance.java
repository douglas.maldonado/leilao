package br.com.leilao;

public class Lance {
    private Usuario usuario;
    private double valorLance;

    public Lance() {}


    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public double getValorLance() {
        return valorLance;
    }

    public void setValorLance(double valorLance) {
        this.valorLance = valorLance;
    }
}
