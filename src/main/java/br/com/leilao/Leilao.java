package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    List<Lance> lanceLista = new ArrayList<Lance>();

    public Leilao(){}

    public List<Lance> adicionarLance(Lance lance){

        for (Lance lances: lanceLista){
            if(lance.getValorLance() < lances.getValorLance()){
                throw new RuntimeException("Valor do lance não pode ser menor");
            }
        }

        lanceLista.add(lance);
        return lanceLista;
    }

    public List<Lance> getLanceLista() {
        return lanceLista;
    }

    public void setLanceLista(List<Lance> lanceLista) {
        this.lanceLista = lanceLista;
    }
}
