package br.com.leilao;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;
    private Lance lance;

    public Leiloeiro(){}

    public Lance retornarMaiorLance(Leilao leilao){
        double valor = 0;

        for (int i=0; i < leilao.getLanceLista().size(); i++){
            if (leilao.getLanceLista().get(i).getValorLance() > valor){
                lance = leilao.getLanceLista().get(i);
                valor = leilao.getLanceLista().get(i).getValorLance();
            }
        }

        return lance;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }
}
